/**
 * <copyright>
 * </copyright>
 *
 * $Id$
 */
package org.openscada.prototype.da.spreadsheet.model.SpreadseetController;

import org.eclipse.emf.ecore.EAttribute;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EEnum;
import org.eclipse.emf.ecore.EPackage;

/**
 * <!-- begin-user-doc -->
 * The <b>Package</b> for the model.
 * It contains accessors for the meta objects to represent
 * <ul>
 *   <li>each class,</li>
 *   <li>each feature of each class,</li>
 *   <li>each enum,</li>
 *   <li>and each data type</li>
 * </ul>
 * <!-- end-user-doc -->
 * @see org.openscada.prototype.da.spreadsheet.model.SpreadseetController.SpreadseetControllerFactory
 * @model kind="package"
 * @generated
 */
public interface SpreadseetControllerPackage extends EPackage
{
    /**
     * The package name.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    String eNAME = "SpreadseetController";

    /**
     * The package namespace URI.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    String eNS_URI = "http://openscada.org/prototype/spreadsheet";

    /**
     * The package namespace name.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    String eNS_PREFIX = "controller";

    /**
     * The singleton instance of the package.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    SpreadseetControllerPackage eINSTANCE = org.openscada.prototype.da.spreadsheet.model.SpreadseetController.impl.SpreadseetControllerPackageImpl.init ();

    /**
     * The meta object id for the '{@link org.openscada.prototype.da.spreadsheet.model.SpreadseetController.impl.ControllerImpl <em>Controller</em>}' class.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @see org.openscada.prototype.da.spreadsheet.model.SpreadseetController.impl.ControllerImpl
     * @see org.openscada.prototype.da.spreadsheet.model.SpreadseetController.impl.SpreadseetControllerPackageImpl#getController()
     * @generated
     */
    int CONTROLLER = 0;

    /**
     * The feature id for the '<em><b>Spreadsheet</b></em>' attribute.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     * @ordered
     */
    int CONTROLLER__SPREADSHEET = 0;

    /**
     * The feature id for the '<em><b>Connection Endpoint</b></em>' attribute.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     * @ordered
     */
    int CONTROLLER__CONNECTION_ENDPOINT = 1;

    /**
     * The feature id for the '<em><b>Naming Mode</b></em>' attribute.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     * @ordered
     */
    int CONTROLLER__NAMING_MODE = 2;

    /**
     * The number of structural features of the '<em>Controller</em>' class.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     * @ordered
     */
    int CONTROLLER_FEATURE_COUNT = 3;

    /**
     * The meta object id for the '{@link org.openscada.prototype.da.spreadsheet.model.SpreadseetController.NamingMode <em>Naming Mode</em>}' enum.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @see org.openscada.prototype.da.spreadsheet.model.SpreadseetController.NamingMode
     * @see org.openscada.prototype.da.spreadsheet.model.SpreadseetController.impl.SpreadseetControllerPackageImpl#getNamingMode()
     * @generated
     */
    int NAMING_MODE = 1;

    /**
     * Returns the meta object for class '{@link org.openscada.prototype.da.spreadsheet.model.SpreadseetController.Controller <em>Controller</em>}'.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @return the meta object for class '<em>Controller</em>'.
     * @see org.openscada.prototype.da.spreadsheet.model.SpreadseetController.Controller
     * @generated
     */
    EClass getController ();

    /**
     * Returns the meta object for the attribute '{@link org.openscada.prototype.da.spreadsheet.model.SpreadseetController.Controller#getSpreadsheet <em>Spreadsheet</em>}'.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @return the meta object for the attribute '<em>Spreadsheet</em>'.
     * @see org.openscada.prototype.da.spreadsheet.model.SpreadseetController.Controller#getSpreadsheet()
     * @see #getController()
     * @generated
     */
    EAttribute getController_Spreadsheet ();

    /**
     * Returns the meta object for the attribute '{@link org.openscada.prototype.da.spreadsheet.model.SpreadseetController.Controller#getConnectionEndpoint <em>Connection Endpoint</em>}'.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @return the meta object for the attribute '<em>Connection Endpoint</em>'.
     * @see org.openscada.prototype.da.spreadsheet.model.SpreadseetController.Controller#getConnectionEndpoint()
     * @see #getController()
     * @generated
     */
    EAttribute getController_ConnectionEndpoint ();

    /**
     * Returns the meta object for the attribute '{@link org.openscada.prototype.da.spreadsheet.model.SpreadseetController.Controller#getNamingMode <em>Naming Mode</em>}'.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @return the meta object for the attribute '<em>Naming Mode</em>'.
     * @see org.openscada.prototype.da.spreadsheet.model.SpreadseetController.Controller#getNamingMode()
     * @see #getController()
     * @generated
     */
    EAttribute getController_NamingMode ();

    /**
     * Returns the meta object for enum '{@link org.openscada.prototype.da.spreadsheet.model.SpreadseetController.NamingMode <em>Naming Mode</em>}'.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @return the meta object for enum '<em>Naming Mode</em>'.
     * @see org.openscada.prototype.da.spreadsheet.model.SpreadseetController.NamingMode
     * @generated
     */
    EEnum getNamingMode ();

    /**
     * Returns the factory that creates the instances of the model.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @return the factory that creates the instances of the model.
     * @generated
     */
    SpreadseetControllerFactory getSpreadseetControllerFactory ();

    /**
     * <!-- begin-user-doc -->
     * Defines literals for the meta objects that represent
     * <ul>
     *   <li>each class,</li>
     *   <li>each feature of each class,</li>
     *   <li>each enum,</li>
     *   <li>and each data type</li>
     * </ul>
     * <!-- end-user-doc -->
     * @generated
     */
    interface Literals
    {
        /**
         * The meta object literal for the '{@link org.openscada.prototype.da.spreadsheet.model.SpreadseetController.impl.ControllerImpl <em>Controller</em>}' class.
         * <!-- begin-user-doc -->
         * <!-- end-user-doc -->
         * @see org.openscada.prototype.da.spreadsheet.model.SpreadseetController.impl.ControllerImpl
         * @see org.openscada.prototype.da.spreadsheet.model.SpreadseetController.impl.SpreadseetControllerPackageImpl#getController()
         * @generated
         */
        EClass CONTROLLER = eINSTANCE.getController ();

        /**
         * The meta object literal for the '<em><b>Spreadsheet</b></em>' attribute feature.
         * <!-- begin-user-doc -->
         * <!-- end-user-doc -->
         * @generated
         */
        EAttribute CONTROLLER__SPREADSHEET = eINSTANCE.getController_Spreadsheet ();

        /**
         * The meta object literal for the '<em><b>Connection Endpoint</b></em>' attribute feature.
         * <!-- begin-user-doc -->
         * <!-- end-user-doc -->
         * @generated
         */
        EAttribute CONTROLLER__CONNECTION_ENDPOINT = eINSTANCE.getController_ConnectionEndpoint ();

        /**
         * The meta object literal for the '<em><b>Naming Mode</b></em>' attribute feature.
         * <!-- begin-user-doc -->
         * <!-- end-user-doc -->
         * @generated
         */
        EAttribute CONTROLLER__NAMING_MODE = eINSTANCE.getController_NamingMode ();

        /**
         * The meta object literal for the '{@link org.openscada.prototype.da.spreadsheet.model.SpreadseetController.NamingMode <em>Naming Mode</em>}' enum.
         * <!-- begin-user-doc -->
         * <!-- end-user-doc -->
         * @see org.openscada.prototype.da.spreadsheet.model.SpreadseetController.NamingMode
         * @see org.openscada.prototype.da.spreadsheet.model.SpreadseetController.impl.SpreadseetControllerPackageImpl#getNamingMode()
         * @generated
         */
        EEnum NAMING_MODE = eINSTANCE.getNamingMode ();

    }

} //SpreadseetControllerPackage
