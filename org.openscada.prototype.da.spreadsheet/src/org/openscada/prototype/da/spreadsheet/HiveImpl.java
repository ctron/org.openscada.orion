/*
 * This file is part of the openSCADA project
 * Copyright (C) 2011-2012 TH4 SYSTEMS GmbH (http://th4-systems.com)
 *
 * openSCADA is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License version 3
 * only, as published by the Free Software Foundation.
 *
 * openSCADA is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License version 3 for more details
 * (a copy is included in the LICENSE file that accompanied this code).
 *
 * You should have received a copy of the GNU Lesser General Public License
 * version 3 along with openSCADA. If not, see
 * <http://opensource.org/licenses/lgpl-3.0.html> for a copy of the LGPLv3 License.
 */

package org.openscada.prototype.da.spreadsheet;

import java.util.HashMap;
import java.util.Map;
import java.util.Set;

import org.eclipse.core.databinding.observable.set.ISetChangeListener;
import org.eclipse.core.databinding.observable.set.SetChangeEvent;
import org.eclipse.core.databinding.observable.set.WritableSet;
import org.eclipse.ui.console.MessageConsole;
import org.eclipse.ui.console.MessageConsoleStream;
import org.openscada.core.InvalidSessionException;
import org.openscada.core.Variant;
import org.openscada.da.core.server.InvalidItemException;
import org.openscada.da.core.server.Session;
import org.openscada.da.server.browser.common.FolderCommon;
import org.openscada.da.server.browser.common.query.GroupFolder;
import org.openscada.da.server.browser.common.query.IDNameProvider;
import org.openscada.da.server.browser.common.query.InvisibleStorage;
import org.openscada.da.server.browser.common.query.SplitGroupProvider;
import org.openscada.da.server.browser.common.query.SplitNameProvider;
import org.openscada.da.server.common.ValidationStrategy;
import org.openscada.da.server.common.impl.HiveCommon;
import org.openscada.utils.collection.MapBuilder;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class HiveImpl extends HiveCommon
{

    private final static Logger logger = LoggerFactory.getLogger ( HiveImpl.class );

    private final WritableSet items;

    private final FolderCommon rootFolder;

    private final ISetChangeListener listener;

    private final InvisibleStorage itemStorage;

    private final WritableSet grantedItems;

    private final MessageConsole messageConsole;

    private MessageConsoleStream messageStream;

    public HiveImpl ( final WritableSet items, final WritableSet grantedItems, final MessageConsole messageConsole )
    {
        this.items = items;
        this.grantedItems = grantedItems;
        this.messageConsole = messageConsole;

        setValidatonStrategy ( ValidationStrategy.GRANT_ALL );

        this.rootFolder = new FolderCommon ();

        setRootFolder ( this.rootFolder );

        this.listener = new ISetChangeListener () {

            @Override
            public void handleSetChange ( final SetChangeEvent event )
            {
                HiveImpl.this.handleSetChange ( event );
            }
        };

        this.itemStorage = new InvisibleStorage ();
        final GroupFolder allItemsFolder = new GroupFolder ( new SplitGroupProvider ( new IDNameProvider (), "\\.", 0, 2 ), new SplitNameProvider ( new IDNameProvider (), "\\.", 0, 2, "." ) );
        this.rootFolder.add ( "all", allItemsFolder, new MapBuilder<String, Variant> ().put ( "description", Variant.valueOf ( "A folder containing the full item space" ) ).getMap () );
        this.itemStorage.addChild ( allItemsFolder );
    }

    @Override
    public String getHiveId ()
    {
        return "org.openscada.prototype.da.spreadsheet";
    }

    @Override
    public void start () throws Exception
    {
        super.start ();

        this.messageStream = this.messageConsole.newMessageStream ();

        this.items.addSetChangeListener ( this.listener );
        for ( final Object object : this.items )
        {
            addItem ( (PrototypeItem)object );
        }
    }

    @Override
    public void stop () throws Exception
    {
        if ( this.messageStream != null )
        {
            try
            {
                this.messageStream.close ();
            }
            catch ( final Exception e )
            {
                logger.warn ( "Failed to close message stream", e );
            }
            this.messageStream = null;
        }

        this.items.removeSetChangeListener ( this.listener );
        super.stop ();
    }

    protected void handleSetChange ( final SetChangeEvent event )
    {
        for ( final Object object : event.diff.getRemovals () )
        {
            removeItem ( (PrototypeItem)object );
        }
        for ( final Object object : event.diff.getAdditions () )
        {
            addItem ( (PrototypeItem)object );
        }
    }

    private final Map<String, SimulationItem> simulationItems = new HashMap<String, SimulationItem> ();

    private void addItem ( final PrototypeItem prototypeItem )
    {
        removeItem ( prototypeItem );

        final SimulationItem item = new SimulationItem ( prototypeItem, this.messageStream );
        registerItem ( item );
        this.simulationItems.put ( prototypeItem.getItemId (), item );
        this.itemStorage.added ( item.getDescriptor () );

        updateGrantedItems ();
    }

    private void removeItem ( final PrototypeItem prototypeItem )
    {
        final SimulationItem simulationItem = this.simulationItems.remove ( prototypeItem.getItemId () );
        if ( simulationItem != null )
        {
            unregisterItem ( simulationItem );
            this.itemStorage.removed ( simulationItem.getDescriptor () );
            simulationItem.dispose ();

            updateGrantedItems ();
        }
    }

    @Override
    public void subscribeItem ( final Session session, final String itemId ) throws InvalidSessionException, InvalidItemException
    {
        super.subscribeItem ( session, itemId );

        updateGrantedItems ();
    }

    @Override
    public void unsubscribeItem ( final Session session, final String itemId ) throws InvalidSessionException, InvalidItemException
    {
        super.unsubscribeItem ( session, itemId );

        updateGrantedItems ();
    }

    private void updateGrantedItems ()
    {
        final Set<String> grantedItems = getGrantedItems ();

        this.grantedItems.getRealm ().asyncExec ( new Runnable () {

            @Override
            public void run ()
            {
                handleSetGrantedItems ( grantedItems );
            }
        } );
    }

    protected void handleSetGrantedItems ( final Set<String> grantedItems )
    {
        try
        {
            this.grantedItems.setStale ( true );
            this.grantedItems.retainAll ( grantedItems );
            this.grantedItems.addAll ( grantedItems );
        }
        finally
        {
            this.grantedItems.setStale ( false );
        }
    }
}
