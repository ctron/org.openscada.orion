/**
 * <copyright>
 * </copyright>
 *
 * $Id$
 */
package org.openscada.prototype.da.spreadsheet.model.SpreadseetController;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Controller</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * <ul>
 *   <li>{@link org.openscada.prototype.da.spreadsheet.model.SpreadseetController.Controller#getSpreadsheet <em>Spreadsheet</em>}</li>
 *   <li>{@link org.openscada.prototype.da.spreadsheet.model.SpreadseetController.Controller#getConnectionEndpoint <em>Connection Endpoint</em>}</li>
 *   <li>{@link org.openscada.prototype.da.spreadsheet.model.SpreadseetController.Controller#getNamingMode <em>Naming Mode</em>}</li>
 * </ul>
 * </p>
 *
 * @see org.openscada.prototype.da.spreadsheet.model.SpreadseetController.SpreadseetControllerPackage#getController()
 * @model
 * @generated
 */
public interface Controller extends EObject
{
    /**
     * Returns the value of the '<em><b>Spreadsheet</b></em>' attribute.
     * <!-- begin-user-doc -->
     * <p>
     * If the meaning of the '<em>Spreadsheet</em>' attribute isn't clear,
     * there really should be more of a description here...
     * </p>
     * <!-- end-user-doc -->
     * @return the value of the '<em>Spreadsheet</em>' attribute.
     * @see #setSpreadsheet(String)
     * @see org.openscada.prototype.da.spreadsheet.model.SpreadseetController.SpreadseetControllerPackage#getController_Spreadsheet()
     * @model
     * @generated
     */
    String getSpreadsheet ();

    /**
     * Sets the value of the '{@link org.openscada.prototype.da.spreadsheet.model.SpreadseetController.Controller#getSpreadsheet <em>Spreadsheet</em>}' attribute.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @param value the new value of the '<em>Spreadsheet</em>' attribute.
     * @see #getSpreadsheet()
     * @generated
     */
    void setSpreadsheet ( String value );

    /**
     * Returns the value of the '<em><b>Connection Endpoint</b></em>' attribute.
     * <!-- begin-user-doc -->
     * <p>
     * If the meaning of the '<em>Connection Endpoint</em>' attribute isn't clear,
     * there really should be more of a description here...
     * </p>
     * <!-- end-user-doc -->
     * @return the value of the '<em>Connection Endpoint</em>' attribute.
     * @see #setConnectionEndpoint(String)
     * @see org.openscada.prototype.da.spreadsheet.model.SpreadseetController.SpreadseetControllerPackage#getController_ConnectionEndpoint()
     * @model
     * @generated
     */
    String getConnectionEndpoint ();

    /**
     * Sets the value of the '{@link org.openscada.prototype.da.spreadsheet.model.SpreadseetController.Controller#getConnectionEndpoint <em>Connection Endpoint</em>}' attribute.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @param value the new value of the '<em>Connection Endpoint</em>' attribute.
     * @see #getConnectionEndpoint()
     * @generated
     */
    void setConnectionEndpoint ( String value );

    /**
     * Returns the value of the '<em><b>Naming Mode</b></em>' attribute.
     * The literals are from the enumeration {@link org.openscada.prototype.da.spreadsheet.model.SpreadseetController.NamingMode}.
     * <!-- begin-user-doc -->
     * <p>
     * If the meaning of the '<em>Naming Mode</em>' attribute isn't clear,
     * there really should be more of a description here...
     * </p>
     * <!-- end-user-doc -->
     * @return the value of the '<em>Naming Mode</em>' attribute.
     * @see org.openscada.prototype.da.spreadsheet.model.SpreadseetController.NamingMode
     * @see #setNamingMode(NamingMode)
     * @see org.openscada.prototype.da.spreadsheet.model.SpreadseetController.SpreadseetControllerPackage#getController_NamingMode()
     * @model required="true"
     * @generated
     */
    NamingMode getNamingMode ();

    /**
     * Sets the value of the '{@link org.openscada.prototype.da.spreadsheet.model.SpreadseetController.Controller#getNamingMode <em>Naming Mode</em>}' attribute.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @param value the new value of the '<em>Naming Mode</em>' attribute.
     * @see org.openscada.prototype.da.spreadsheet.model.SpreadseetController.NamingMode
     * @see #getNamingMode()
     * @generated
     */
    void setNamingMode ( NamingMode value );

} // Controller
