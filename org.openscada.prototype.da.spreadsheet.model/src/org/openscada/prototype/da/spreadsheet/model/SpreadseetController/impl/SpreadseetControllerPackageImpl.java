/**
 * <copyright>
 * </copyright>
 *
 * $Id$
 */
package org.openscada.prototype.da.spreadsheet.model.SpreadseetController.impl;

import org.eclipse.emf.ecore.EAttribute;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EEnum;
import org.eclipse.emf.ecore.EPackage;

import org.eclipse.emf.ecore.impl.EPackageImpl;

import org.openscada.prototype.da.spreadsheet.model.SpreadseetController.Controller;
import org.openscada.prototype.da.spreadsheet.model.SpreadseetController.NamingMode;
import org.openscada.prototype.da.spreadsheet.model.SpreadseetController.SpreadseetControllerFactory;
import org.openscada.prototype.da.spreadsheet.model.SpreadseetController.SpreadseetControllerPackage;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model <b>Package</b>.
 * <!-- end-user-doc -->
 * @generated
 */
public class SpreadseetControllerPackageImpl extends EPackageImpl implements SpreadseetControllerPackage
{
    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    private EClass controllerEClass = null;

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    private EEnum namingModeEEnum = null;

    /**
     * Creates an instance of the model <b>Package</b>, registered with
     * {@link org.eclipse.emf.ecore.EPackage.Registry EPackage.Registry} by the package
     * package URI value.
     * <p>Note: the correct way to create the package is via the static
     * factory method {@link #init init()}, which also performs
     * initialization of the package, or returns the registered package,
     * if one already exists.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @see org.eclipse.emf.ecore.EPackage.Registry
     * @see org.openscada.prototype.da.spreadsheet.model.SpreadseetController.SpreadseetControllerPackage#eNS_URI
     * @see #init()
     * @generated
     */
    private SpreadseetControllerPackageImpl ()
    {
        super ( eNS_URI, SpreadseetControllerFactory.eINSTANCE );
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    private static boolean isInited = false;

    /**
     * Creates, registers, and initializes the <b>Package</b> for this model, and for any others upon which it depends.
     * 
     * <p>This method is used to initialize {@link SpreadseetControllerPackage#eINSTANCE} when that field is accessed.
     * Clients should not invoke it directly. Instead, they should simply access that field to obtain the package.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @see #eNS_URI
     * @see #createPackageContents()
     * @see #initializePackageContents()
     * @generated
     */
    public static SpreadseetControllerPackage init ()
    {
        if ( isInited )
            return (SpreadseetControllerPackage)EPackage.Registry.INSTANCE.getEPackage ( SpreadseetControllerPackage.eNS_URI );

        // Obtain or create and register package
        SpreadseetControllerPackageImpl theSpreadseetControllerPackage = (SpreadseetControllerPackageImpl) ( EPackage.Registry.INSTANCE.get ( eNS_URI ) instanceof SpreadseetControllerPackageImpl ? EPackage.Registry.INSTANCE.get ( eNS_URI ) : new SpreadseetControllerPackageImpl () );

        isInited = true;

        // Create package meta-data objects
        theSpreadseetControllerPackage.createPackageContents ();

        // Initialize created meta-data
        theSpreadseetControllerPackage.initializePackageContents ();

        // Mark meta-data to indicate it can't be changed
        theSpreadseetControllerPackage.freeze ();

        // Update the registry and return the package
        EPackage.Registry.INSTANCE.put ( SpreadseetControllerPackage.eNS_URI, theSpreadseetControllerPackage );
        return theSpreadseetControllerPackage;
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    public EClass getController ()
    {
        return controllerEClass;
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    public EAttribute getController_Spreadsheet ()
    {
        return (EAttribute)controllerEClass.getEStructuralFeatures ().get ( 0 );
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    public EAttribute getController_ConnectionEndpoint ()
    {
        return (EAttribute)controllerEClass.getEStructuralFeatures ().get ( 1 );
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    public EAttribute getController_NamingMode ()
    {
        return (EAttribute)controllerEClass.getEStructuralFeatures ().get ( 2 );
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    public EEnum getNamingMode ()
    {
        return namingModeEEnum;
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    public SpreadseetControllerFactory getSpreadseetControllerFactory ()
    {
        return (SpreadseetControllerFactory)getEFactoryInstance ();
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    private boolean isCreated = false;

    /**
     * Creates the meta-model objects for the package.  This method is
     * guarded to have no affect on any invocation but its first.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    public void createPackageContents ()
    {
        if ( isCreated )
            return;
        isCreated = true;

        // Create classes and their features
        controllerEClass = createEClass ( CONTROLLER );
        createEAttribute ( controllerEClass, CONTROLLER__SPREADSHEET );
        createEAttribute ( controllerEClass, CONTROLLER__CONNECTION_ENDPOINT );
        createEAttribute ( controllerEClass, CONTROLLER__NAMING_MODE );

        // Create enums
        namingModeEEnum = createEEnum ( NAMING_MODE );
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    private boolean isInitialized = false;

    /**
     * Complete the initialization of the package and its meta-model.  This
     * method is guarded to have no affect on any invocation but its first.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    public void initializePackageContents ()
    {
        if ( isInitialized )
            return;
        isInitialized = true;

        // Initialize package
        setName ( eNAME );
        setNsPrefix ( eNS_PREFIX );
        setNsURI ( eNS_URI );

        // Create type parameters

        // Set bounds for type parameters

        // Add supertypes to classes

        // Initialize classes and features; add operations and parameters
        initEClass ( controllerEClass, Controller.class, "Controller", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS );
        initEAttribute ( getController_Spreadsheet (), ecorePackage.getEString (), "spreadsheet", null, 0, 1, Controller.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED );
        initEAttribute ( getController_ConnectionEndpoint (), ecorePackage.getEString (), "connectionEndpoint", null, 0, 1, Controller.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED );
        initEAttribute ( getController_NamingMode (), this.getNamingMode (), "namingMode", null, 1, 1, Controller.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED );

        // Initialize enums and add enum literals
        initEEnum ( namingModeEEnum, NamingMode.class, "NamingMode" );
        addEEnumLiteral ( namingModeEEnum, NamingMode.ALIAS );
        addEEnumLiteral ( namingModeEEnum, NamingMode.INTERNAL );

        // Create resource
        createResource ( eNS_URI );
    }

} //SpreadseetControllerPackageImpl
