/*
 * This file is part of the openSCADA project
 * Copyright (C) 2011-2012 TH4 SYSTEMS GmbH (http://th4-systems.com)
 *
 * openSCADA is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License version 3
 * only, as published by the Free Software Foundation.
 *
 * openSCADA is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License version 3 for more details
 * (a copy is included in the LICENSE file that accompanied this code).
 *
 * You should have received a copy of the GNU Lesser General Public License
 * version 3 along with openSCADA. If not, see
 * <http://opensource.org/licenses/lgpl-3.0.html> for a copy of the LGPLv3 License.
 */

package org.openscada.prototype.da.spreadsheet;

import java.io.IOException;
import java.util.Collection;
import java.util.LinkedList;
import java.util.List;

import org.eclipse.core.databinding.DataBindingContext;
import org.eclipse.core.databinding.UpdateValueStrategy;
import org.eclipse.core.databinding.beans.BeanProperties;
import org.eclipse.core.databinding.conversion.Converter;
import org.eclipse.core.databinding.observable.value.IObservableValue;
import org.eclipse.core.databinding.observable.value.IValueChangeListener;
import org.eclipse.core.databinding.observable.value.ValueChangeEvent;
import org.eclipse.core.databinding.property.Properties;
import org.eclipse.core.databinding.property.value.IValueProperty;
import org.eclipse.core.resources.IFile;
import org.eclipse.core.resources.IResource;
import org.eclipse.core.resources.IStorage;
import org.eclipse.core.resources.ResourcesPlugin;
import org.eclipse.core.runtime.CoreException;
import org.eclipse.core.runtime.IProgressMonitor;
import org.eclipse.core.runtime.Path;
import org.eclipse.core.variables.VariablesPlugin;
import org.eclipse.debug.ui.StringVariableSelectionDialog;
import org.eclipse.emf.databinding.EMFObservables;
import org.eclipse.jface.databinding.swt.SWTObservables;
import org.eclipse.jface.databinding.swt.WidgetProperties;
import org.eclipse.jface.databinding.viewers.CellEditorProperties;
import org.eclipse.jface.databinding.viewers.ObservableMapCellLabelProvider;
import org.eclipse.jface.databinding.viewers.ObservableSetContentProvider;
import org.eclipse.jface.databinding.viewers.ViewersObservables;
import org.eclipse.jface.viewers.ArrayContentProvider;
import org.eclipse.jface.viewers.CellEditor;
import org.eclipse.jface.viewers.CellLabelProvider;
import org.eclipse.jface.viewers.CheckboxCellEditor;
import org.eclipse.jface.viewers.ColumnViewer;
import org.eclipse.jface.viewers.ColumnWeightData;
import org.eclipse.jface.viewers.ComboViewer;
import org.eclipse.jface.viewers.EditingSupport;
import org.eclipse.jface.viewers.TableLayout;
import org.eclipse.jface.viewers.TableViewer;
import org.eclipse.jface.viewers.TableViewerColumn;
import org.eclipse.jface.viewers.TextCellEditor;
import org.eclipse.jface.viewers.Viewer;
import org.eclipse.jface.viewers.ViewerCell;
import org.eclipse.jface.viewers.ViewerComparator;
import org.eclipse.jface.window.Window;
import org.eclipse.swt.SWT;
import org.eclipse.swt.custom.CTabFolder;
import org.eclipse.swt.custom.CTabItem;
import org.eclipse.swt.events.SelectionAdapter;
import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.layout.FillLayout;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.FileDialog;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.Text;
import org.eclipse.ui.IEditorInput;
import org.eclipse.ui.IEditorSite;
import org.eclipse.ui.IFileEditorInput;
import org.eclipse.ui.IStorageEditorInput;
import org.eclipse.ui.PartInitException;
import org.eclipse.ui.console.ConsolePlugin;
import org.eclipse.ui.console.IConsole;
import org.eclipse.ui.console.MessageConsole;
import org.eclipse.ui.part.EditorPart;
import org.eclipse.ui.statushandlers.StatusManager;
import org.openscada.core.ConnectionInformation;
import org.openscada.core.Variant;
import org.openscada.core.VariantEditor;
import org.openscada.deploy.iolist.model.Item;
import org.openscada.deploy.iolist.utils.ItemListReader;
import org.openscada.prototype.da.spreadsheet.model.SpreadseetController.Controller;
import org.openscada.prototype.da.spreadsheet.model.SpreadseetController.NamingMode;
import org.openscada.prototype.da.spreadsheet.model.SpreadseetController.SpreadseetControllerPackage;
import org.openscada.ui.databinding.cell.DefaultEditingSupport;
import org.openscada.ui.databinding.observable.InvertedObserableValue;
import org.openscada.ui.utils.status.StatusHelper;

public class ControllerEditorPart extends EditorPart
{

    public static class CheckboxEditorSupport extends EditingSupport
    {
        private final CellEditor cellEditor;

        private final IValueProperty elementProperty;

        public CheckboxEditorSupport ( final ColumnViewer viewer, final CellEditor cellEditor, final IValueProperty elementProperty )
        {
            super ( viewer );
            this.cellEditor = cellEditor;
            this.elementProperty = elementProperty;
        }

        @Override
        protected void setValue ( final Object element, final Object value )
        {
            final IObservableValue observable = this.elementProperty.observe ( element );
            observable.setValue ( value );
            observable.dispose ();
        }

        @Override
        protected Object getValue ( final Object element )
        {
            final IObservableValue observable = this.elementProperty.observe ( element );
            final Object result = observable.getValue ();
            observable.dispose ();
            return result;
        }

        @Override
        protected CellEditor getCellEditor ( final Object element )
        {
            return this.cellEditor;
        }

        @Override
        protected boolean canEdit ( final Object element )
        {
            return true;
        }
    }

    public class SetDirtyListener implements IValueChangeListener
    {
        @Override
        public void handleValueChange ( final ValueChangeEvent event )
        {
            setDirty ( true );
        }
    }

    private DataBindingContext dbc;

    private Controller controller;

    private boolean dirty = false;

    private Button loadButton;

    private Text controllerText;

    private TableViewer viewer;

    private ServerController serverController;

    private MessageConsole messageConsole;

    public ControllerEditorPart ()
    {
    }

    @Override
    public void doSave ( final IProgressMonitor monitor )
    {
        try
        {
            ( (IFileEditorInput)getEditorInput () ).getFile ().setContents ( Helper.saveController ( this.controller ), 0, monitor );
            setDirty ( false );
        }
        catch ( final CoreException e )
        {
            StatusManager.getManager ().handle ( e.getStatus (), StatusManager.BLOCK );
        }
        catch ( final IOException e )
        {
            StatusManager.getManager ().handle ( StatusHelper.convertStatus ( Activator.PLUGIN_ID, e ), StatusManager.BLOCK );
        }
        finally
        {
            monitor.done ();
        }
    }

    @Override
    public void doSaveAs ()
    {
    }

    @Override
    public void init ( final IEditorSite site, final IEditorInput input ) throws PartInitException
    {
        setPartName ( input.getName () );
        setSite ( site );
        setInput ( input );
    }

    @Override
    protected void setInput ( final IEditorInput input )
    {
        try
        {
            final IStorage storage = ( (IStorageEditorInput)input ).getStorage ();
            loadContents ( storage );
        }
        catch ( final CoreException e )
        {
            StatusManager.getManager ().handle ( e.getStatus () );
        }
        super.setInput ( input );
    }

    private void loadContents ( final IStorage storage ) throws CoreException
    {
        try
        {
            this.controller = Helper.loadController ( storage.getContents () );
        }
        catch ( final Exception e )
        {
            throw new CoreException ( StatusHelper.convertStatus ( Activator.PLUGIN_ID, e ) );
        }
    }

    @Override
    public boolean isDirty ()
    {
        return this.dirty;
    }

    @Override
    public boolean isSaveAsAllowed ()
    {
        return false;
    }

    @Override
    public void createPartControl ( final Composite parent )
    {
        this.dbc = new DataBindingContext ();

        createConsole ();

        this.serverController = new ServerController ( SWTObservables.getRealm ( parent.getDisplay () ) );

        final Composite wrapper = new Composite ( parent, SWT.NONE );
        parent.setLayout ( new FillLayout () );

        final GridLayout layout = new GridLayout ( 2, false );
        layout.marginHeight = layout.marginWidth = 0;
        wrapper.setLayout ( layout );

        // create spreadsheet file

        {
            final Label label = new Label ( wrapper, SWT.NONE );
            label.setText ( "Spreadsheet File:" );

            final Composite sub = new Composite ( wrapper, SWT.NONE );
            sub.setLayout ( new GridLayout ( 4, false ) );
            sub.setLayoutData ( new GridData ( SWT.FILL, SWT.FILL, true, false ) );

            this.controllerText = new Text ( sub, SWT.BORDER );
            this.controllerText.setLayoutData ( new GridData ( SWT.FILL, SWT.CENTER, true, false ) );

            this.dbc.bindValue ( SWTObservables.observeText ( this.controllerText, SWT.Modify ), EMFObservables.observeValue ( this.controller, SpreadseetControllerPackage.Literals.CONTROLLER__SPREADSHEET ) );

            {
                final Button button = new Button ( sub, SWT.PUSH );
                button.setText ( "File System…" );
                button.addSelectionListener ( new SelectionAdapter () {
                    @Override
                    public void widgetSelected ( final SelectionEvent e )
                    {
                        chooseFile ();
                    }
                } );
            }
            {
                final Button button = new Button ( sub, SWT.PUSH );
                button.setText ( "Variables…" );
                button.addSelectionListener ( new SelectionAdapter () {
                    @Override
                    public void widgetSelected ( final SelectionEvent e )
                    {
                        chooseVariable ();
                    }
                } );
            }

            this.loadButton = new Button ( sub, SWT.PUSH );
            this.loadButton.setText ( "Load" );
            this.loadButton.addSelectionListener ( new SelectionAdapter () {
                @Override
                public void widgetSelected ( final SelectionEvent e )
                {
                    loadSpreadsheet ();
                }
            } );

        }

        // create connection information

        {

            final Label label = new Label ( wrapper, SWT.NONE );
            label.setText ( "Connection Information:" );

            final Composite sub = new Composite ( wrapper, SWT.NONE );
            sub.setLayout ( new GridLayout ( 3, false ) );
            sub.setLayoutData ( new GridData ( SWT.FILL, SWT.FILL, true, false ) );

            final Text text = new Text ( sub, SWT.SINGLE | SWT.BORDER );
            text.setLayoutData ( new GridData ( SWT.FILL, SWT.CENTER, true, false ) );
            this.dbc.bindValue ( SWTObservables.observeText ( text, SWT.Modify ), EMFObservables.observeValue ( this.controller, SpreadseetControllerPackage.Literals.CONTROLLER__CONNECTION_ENDPOINT ) );

            final Button startButton = new Button ( sub, SWT.PUSH );
            startButton.setText ( "Start" );
            startButton.addSelectionListener ( new SelectionAdapter () {
                @Override
                public void widgetSelected ( final SelectionEvent e )
                {
                    startServer ();
                }
            } );
            final Button stopButton = new Button ( sub, SWT.PUSH );
            stopButton.setText ( "Stop" );
            stopButton.addSelectionListener ( new SelectionAdapter () {
                @Override
                public void widgetSelected ( final SelectionEvent e )
                {
                    stopServer ();
                }
            } );

            // enablement
            this.dbc.bindValue ( SWTObservables.observeEnabled ( text ), new InvertedObserableValue ( this.serverController.getRunningState () ) );
            this.dbc.bindValue ( SWTObservables.observeEnabled ( startButton ), new InvertedObserableValue ( this.serverController.getRunningState () ) );
            this.dbc.bindValue ( SWTObservables.observeEnabled ( stopButton ), this.serverController.getRunningState () );
        }

        {
            final Label label = new Label ( wrapper, SWT.NONE );
            label.setText ( "Naming mode:" );

            final ComboViewer combo = new ComboViewer ( wrapper, SWT.READ_ONLY | SWT.DROP_DOWN );

            combo.setContentProvider ( new ArrayContentProvider () );
            combo.setInput ( NamingMode.values () );

            this.dbc.bindValue ( ViewersObservables.observeSingleSelection ( combo ), EMFObservables.observeValue ( this.controller, SpreadseetControllerPackage.Literals.CONTROLLER__NAMING_MODE ) );
        }

        // tab folder

        final CTabFolder tabFolder = new CTabFolder ( wrapper, SWT.BOTTOM | SWT.FLAT );

        {
            final GridData gd = new GridData ( SWT.FILL, SWT.FILL, true, true );
            gd.horizontalSpan = 2;
            tabFolder.setLayoutData ( gd );
        }

        {
            // create table
            final CTabItem itemTab = new CTabItem ( tabFolder, SWT.NONE );
            itemTab.setText ( "Items" );
            final Composite itemTabControl = new Composite ( tabFolder, SWT.NONE );
            itemTabControl.setLayout ( new FillLayout () );
            itemTab.setControl ( itemTabControl );

            createItemTable ( itemTabControl );
        }

        {
            // create table
            final CTabItem grantedTab = new CTabItem ( tabFolder, SWT.NONE );
            grantedTab.setText ( "Granted" );
            final Composite grantedTabControl = new Composite ( tabFolder, SWT.NONE );
            grantedTabControl.setLayout ( new FillLayout () );
            grantedTab.setControl ( grantedTabControl );
            createGrantedTable ( grantedTabControl );
        }

        // select first tab
        tabFolder.setSelection ( 0 );

        // dirty listeners
        EMFObservables.observeValue ( this.controller, SpreadseetControllerPackage.Literals.CONTROLLER__SPREADSHEET ).addValueChangeListener ( new SetDirtyListener () );
        EMFObservables.observeValue ( this.controller, SpreadseetControllerPackage.Literals.CONTROLLER__CONNECTION_ENDPOINT ).addValueChangeListener ( new SetDirtyListener () );
        EMFObservables.observeValue ( this.controller, SpreadseetControllerPackage.Literals.CONTROLLER__NAMING_MODE ).addValueChangeListener ( new SetDirtyListener () );
        EMFObservables.observeValue ( this.controller, SpreadseetControllerPackage.Literals.CONTROLLER__NAMING_MODE ).addValueChangeListener ( new IValueChangeListener () {

            @Override
            public void handleValueChange ( final ValueChangeEvent event )
            {
                System.out.println ( "attribute: " + event.getObservableValue () );
            }
        } );
    }

    private void createGrantedTable ( final Composite wrapper )
    {
        final TableViewer grantedViewer = new TableViewer ( wrapper, SWT.FULL_SELECTION );
        final TableLayout tableLayout = new TableLayout ();
        grantedViewer.getTable ().setLayout ( tableLayout );

        final ObservableSetContentProvider contentProvider = new ObservableSetContentProvider ();
        grantedViewer.setContentProvider ( contentProvider );

        grantedViewer.setInput ( this.serverController.getGrantedItems () );
        grantedViewer.setComparator ( new ViewerComparator () {
            @Override
            public int compare ( final Viewer viewer, final Object e1, final Object e2 )
            {
                return ( (PrototypeItem)e1 ).getItemId ().compareTo ( ( (PrototypeItem)e2 ).getItemId () );
            }
        } );
    }

    private void createItemTable ( final Composite wrapper )
    {
        this.viewer = new TableViewer ( wrapper, SWT.FULL_SELECTION );
        final TableLayout tableLayout = new TableLayout ();
        this.viewer.getTable ().setLayout ( tableLayout );
        this.viewer.getTable ().setHeaderVisible ( true );

        final ObservableSetContentProvider contentProvider = new ObservableSetContentProvider ();
        this.viewer.setContentProvider ( contentProvider );

        {
            final TableViewerColumn col = new TableViewerColumn ( this.viewer, SWT.NONE );
            col.getColumn ().setText ( "Item Id" );
            tableLayout.addColumnData ( new ColumnWeightData ( 100 ) );
            col.setLabelProvider ( new CellLabelProvider () {

                @Override
                public void update ( final ViewerCell cell )
                {
                    cell.setText ( ( (PrototypeItem)cell.getElement () ).getItemId () );
                }
            } );
        }
        {
            final TableViewerColumn col = new TableViewerColumn ( this.viewer, SWT.NONE );
            col.getColumn ().setText ( "Simulation Value" );
            tableLayout.addColumnData ( new ColumnWeightData ( 50 ) );

            col.setLabelProvider ( new ObservableMapCellLabelProvider ( Properties.observeEach ( contentProvider.getKnownElements (), BeanProperties.values ( PrototypeItem.class, new String[] { PrototypeItem.PROP_SIMULATION_VALUE } ) )[0] ) );

            final TextCellEditor cellEditor = new TextCellEditor ( this.viewer.getTable () );

            final IValueProperty cellEditorProperty = CellEditorProperties.control ().value ( WidgetProperties.text ( SWT.Modify ) );
            final IValueProperty elementProperty = BeanProperties.value ( PrototypeItem.PROP_SIMULATION_VALUE );

            col.setEditingSupport ( new DefaultEditingSupport ( this.viewer, this.dbc, cellEditorProperty, cellEditor, elementProperty ) {
                @Override
                protected UpdateValueStrategy createTargetToModelStrategy ()
                {
                    final UpdateValueStrategy result = new UpdateValueStrategy ();
                    result.setConverter ( new Converter ( String.class, Variant.class ) {

                        @Override
                        public Object convert ( final Object fromObject )
                        {
                            return VariantEditor.toVariant ( (String)fromObject );
                        }
                    } );
                    return result;
                }
            } );
        }
        {
            createFlagColumn ( tableLayout, contentProvider, "Subscribed", PrototypeItem.PROP_SUBSCRIBED, false );
            createFlagColumn ( tableLayout, contentProvider, "Error", PrototypeItem.PROP_SIMULATION_ERROR_FLAG, true );
            createFlagColumn ( tableLayout, contentProvider, "Alarm", PrototypeItem.PROP_SIMULATION_ALARM_FLAG, true );
            createFlagColumn ( tableLayout, contentProvider, "Manual", PrototypeItem.PROP_SIMULATION_MANUAL_FLAG, true );
            createFlagColumn ( tableLayout, contentProvider, "Block", PrototypeItem.PROP_SIMULATION_BLOCK_FLAG, true );
            createFlagColumn ( tableLayout, contentProvider, "Ack", PrototypeItem.PROP_SIMULATION_ACK_FLAG, true );
        }

        this.viewer.setInput ( this.serverController.getItems () );
        this.viewer.setComparator ( new ViewerComparator () {
            @Override
            public int compare ( final Viewer viewer, final Object e1, final Object e2 )
            {
                return ( (PrototypeItem)e1 ).getItemId ().compareTo ( ( (PrototypeItem)e2 ).getItemId () );
            }
        } );
    }

    private void createFlagColumn ( final TableLayout tableLayout, final ObservableSetContentProvider contentProvider, final String label, final String propertyName, final boolean editable )
    {
        final TableViewerColumn col = new TableViewerColumn ( this.viewer, SWT.NONE );
        col.getColumn ().setText ( label );
        tableLayout.addColumnData ( new ColumnWeightData ( 10 ) );

        col.setLabelProvider ( new ObservableMapCellLabelProvider ( Properties.observeEach ( contentProvider.getKnownElements (), BeanProperties.values ( PrototypeItem.class, new String[] { propertyName } ) )[0] ) );

        if ( editable )
        {
            createCheckCellEditor ( col, BeanProperties.value ( propertyName ) );
        }
    }

    private void createCheckCellEditor ( final TableViewerColumn col, final IValueProperty elementProperty )
    {
        final CellEditor cellEditor = new CheckboxCellEditor ();
        col.setEditingSupport ( new CheckboxEditorSupport ( col.getViewer (), cellEditor, elementProperty ) );
    }

    protected void createConsole ()
    {
        if ( this.messageConsole == null )
        {
            this.messageConsole = new MessageConsole ( getPartName (), null, null, false );
            ConsolePlugin.getDefault ().getConsoleManager ().addConsoles ( new IConsole[] { this.messageConsole } );
        }
    }

    protected void closeConsole ()
    {
        if ( this.messageConsole != null )
        {
            ConsolePlugin.getDefault ().getConsoleManager ().removeConsoles ( new IConsole[] { this.messageConsole } );
            this.messageConsole.destroy ();
            this.messageConsole = null;
        }
    }

    protected void stopServer ()
    {
        this.serverController.stop ();
    }

    protected void startServer ()
    {
        this.serverController.setConnectionInformation ( ConnectionInformation.fromURI ( this.controller.getConnectionEndpoint () ) );
        this.serverController.start ( this.messageConsole );
    }

    protected void chooseVariable ()
    {
        final StringVariableSelectionDialog dlg = new StringVariableSelectionDialog ( getSite ().getShell () );
        if ( dlg.open () == Window.OK )
        {
            setDirty ( true );
            this.controllerText.insert ( dlg.getVariableExpression () );
        }
    }

    protected void loadSpreadsheet ()
    {
        final String expression = this.controller.getSpreadsheet ();
        try
        {
            final String path = VariablesPlugin.getDefault ().getStringVariableManager ().performStringSubstitution ( expression );

            final IResource resource = ResourcesPlugin.getWorkspace ().getRoot ().getFileForLocation ( new Path ( path ) );

            final Collection<PrototypeItem> items = convertItems ( loadItems ( (IFile)resource ) );
            this.serverController.setItems ( items );
        }
        catch ( final CoreException e )
        {
            StatusManager.getManager ().handle ( e.getStatus (), StatusManager.BLOCK );
        }
        catch ( final Exception e )
        {
            StatusManager.getManager ().handle ( StatusHelper.convertStatus ( Activator.PLUGIN_ID, e ), StatusManager.BLOCK );
        }
    }

    private List<PrototypeItem> convertItems ( final List<Item> items )
    {
        final List<PrototypeItem> result = new LinkedList<PrototypeItem> ();

        for ( final Item item : items )
        {
            switch ( this.controller.getNamingMode () )
            {
                case ALIAS:
                    result.add ( new PrototypeItem ( item, item.getAlias () ) );
                    break;
                case INTERNAL:
                    if ( item.getName () != null )
                    {
                        result.add ( new PrototypeItem ( item, item.getName () ) );
                    }
                    break;
            }

        }

        return result;
    }

    private List<Item> loadItems ( final IFile file ) throws Exception, CoreException
    {
        if ( file.getName ().toUpperCase ().endsWith ( ".ODS" ) )
        {
            return new ItemListReader ( file.getContents (), file.getName () ).getItems ();
        }
        else
        {
            // FIXME: should also load ".iolist" files directly
            throw new IllegalArgumentException ( "Unknown file type" );
        }
    }

    @Override
    public void dispose ()
    {
        this.serverController.dispose ();
        this.dbc.dispose ();

        closeConsole ();

        super.dispose ();
    }

    protected void chooseFile ()
    {
        final FileDialog dlg = new FileDialog ( getSite ().getShell (), SWT.OPEN );
        final String result = dlg.open ();
        if ( result != null )
        {
            this.controllerText.setText ( result );
        }
    }

    protected void setDirty ( final boolean dirty )
    {
        this.dirty = dirty;
        firePropertyChange ( PROP_DIRTY );
    }

    @Override
    public void setFocus ()
    {
        this.viewer.getControl ().setFocus ();
    }

}
