/*
 * This file is part of the openSCADA project
 * Copyright (C) 2011-2012 TH4 SYSTEMS GmbH (http://th4-systems.com)
 *
 * openSCADA is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License version 3
 * only, as published by the Free Software Foundation.
 *
 * openSCADA is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License version 3 for more details
 * (a copy is included in the LICENSE file that accompanied this code).
 *
 * You should have received a copy of the GNU Lesser General Public License
 * version 3 along with openSCADA. If not, see
 * <http://opensource.org/licenses/lgpl-3.0.html> for a copy of the LGPLv3 License.
 */

package org.openscada.prototype.da.spreadsheet;

import org.openscada.core.Variant;
import org.openscada.core.VariantEditor;
import org.openscada.deploy.iolist.model.Item;
import org.openscada.utils.beans.AbstractPropertyChange;

public class PrototypeItem extends AbstractPropertyChange
{
    public static final String PROP_SIMULATION_VALUE = "simulationValue";

    public static final String PROP_SIMULATION_ERROR_FLAG = "simulationErrorFlag";

    public static final String PROP_SIMULATION_ALARM_FLAG = "simulationAlarmFlag";

    public static final String PROP_SIMULATION_MANUAL_FLAG = "simulationManualFlag";

    public static final String PROP_SIMULATION_BLOCK_FLAG = "simulationBlockFlag";

    public static final String PROP_SIMULATION_ACK_FLAG = "simulationAckFlag";

    public static final String PROP_SUBSCRIBED = "subscribed";

    private final Item item;

    private final String itemId;

    private Variant simulationValue = Variant.NULL;

    private boolean simulationErrorFlag = false;

    private boolean simulationAlarmFlag = false;

    private boolean simulationManualFlag = false;

    private boolean simulationBlockFlag = false;

    private boolean simulationAckFlag = false;

    private boolean subscribed = false;

    public PrototypeItem ( final Item item, final String itemId )
    {
        this.item = item;
        this.itemId = itemId;
        this.simulationValue = VariantEditor.toVariant ( item.getSimulationValue () );
    }

    public Item getItem ()
    {
        return this.item;
    }

    public String getItemId ()
    {
        return this.itemId;
    }

    @Override
    public int hashCode ()
    {
        final int prime = 31;
        int result = 1;
        result = prime * result + ( this.itemId == null ? 0 : this.itemId.hashCode () );
        return result;
    }

    @Override
    public boolean equals ( final Object obj )
    {
        if ( this == obj )
        {
            return true;
        }
        if ( obj == null )
        {
            return false;
        }
        if ( getClass () != obj.getClass () )
        {
            return false;
        }
        final PrototypeItem other = (PrototypeItem)obj;
        if ( this.itemId == null )
        {
            if ( other.itemId != null )
            {
                return false;
            }
        }
        else if ( !this.itemId.equals ( other.itemId ) )
        {
            return false;
        }
        return true;
    }

    public Variant getSimulationValue ()
    {
        return this.simulationValue;
    }

    public void setSimulationValue ( final Variant simulationValue )
    {
        firePropertyChange ( PROP_SIMULATION_VALUE, this.simulationValue, this.simulationValue = simulationValue );
    }

    public boolean isSimulationErrorFlag ()
    {
        return this.simulationErrorFlag;
    }

    public void setSimulationErrorFlag ( final boolean simulationErrorFlag )
    {
        firePropertyChange ( PROP_SIMULATION_ERROR_FLAG, this.simulationErrorFlag, this.simulationErrorFlag = simulationErrorFlag );
    }

    public boolean isSimulationAlarmFlag ()
    {
        return this.simulationAlarmFlag;
    }

    public void setSimulationAlarmFlag ( final boolean simulationAlarmFlag )
    {
        firePropertyChange ( PROP_SIMULATION_ALARM_FLAG, this.simulationAlarmFlag, this.simulationAlarmFlag = simulationAlarmFlag );
    }

    public boolean isSimulationManualFlag ()
    {
        return this.simulationManualFlag;
    }

    public void setSimulationManualFlag ( final boolean simulationManualFlag )
    {
        firePropertyChange ( PROP_SIMULATION_MANUAL_FLAG, this.simulationManualFlag, this.simulationManualFlag = simulationManualFlag );
    }

    public boolean isSimulationBlockFlag ()
    {
        return this.simulationBlockFlag;
    }

    public void setSimulationBlockFlag ( final boolean simulationBlockFlag )
    {
        firePropertyChange ( PROP_SIMULATION_BLOCK_FLAG, this.simulationBlockFlag, this.simulationBlockFlag = simulationBlockFlag );
    }

    public boolean isSimulationAckFlag ()
    {
        return this.simulationAckFlag;
    }

    public void setSimulationAckFlag ( final boolean simulationAckFlag )
    {
        firePropertyChange ( PROP_SIMULATION_ACK_FLAG, this.simulationAckFlag, this.simulationAckFlag = simulationAckFlag );
    }

    public boolean isSubscribed ()
    {
        return this.subscribed;
    }

    public void setSubscribed ( final boolean subscribed )
    {
        firePropertyChange ( PROP_SUBSCRIBED, this.subscribed, this.subscribed = subscribed );
    }

}
