/*
 * This file is part of the openSCADA project
 * 
 * Copyright (C) 2013 Jens Reimann (ctron@dentrassi.de)
 *
 * openSCADA is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License version 3
 * only, as published by the Free Software Foundation.
 *
 * openSCADA is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License version 3 for more details
 * (a copy is included in the LICENSE file that accompanied this code).
 *
 * You should have received a copy of the GNU Lesser General Public License
 * version 3 along with openSCADA. If not, see
 * <http://opensource.org/licenses/lgpl-3.0.html> for a copy of the LGPLv3 License.
 */

package org.openscada.protocol.ngp.model.additions;

import org.eclipse.core.commands.ExecutionEvent;
import org.eclipse.core.commands.ExecutionException;
import org.eclipse.emf.edit.command.SetCommand;
import org.eclipse.emf.edit.domain.AdapterFactoryEditingDomain;
import org.eclipse.emf.edit.domain.EditingDomain;
import org.eclipse.emf.edit.domain.IEditingDomainProvider;
import org.eclipse.ui.IEditorPart;
import org.openscada.protocol.ngp.model.Protocol.Attribute;
import org.openscada.protocol.ngp.model.Protocol.ProtocolPackage;
import org.openscada.ui.databinding.AbstractSelectionHandler;
import org.openscada.ui.databinding.SelectionHelper;

public class FillFieldNumbersHandler extends AbstractSelectionHandler
{

    @Override
    public Object execute ( final ExecutionEvent event ) throws ExecutionException
    {

        final IEditorPart editor = getActivePage ().getActiveEditor ();

        byte b = (byte)1;
        for ( final Attribute attribute : SelectionHelper.iterable ( getSelection (), Attribute.class ) )
        {
            EditingDomain domain = AdapterFactoryEditingDomain.getEditingDomainFor ( attribute );

            if ( domain == null && editor instanceof IEditingDomainProvider )
            {
                domain = ( (IEditingDomainProvider)editor ).getEditingDomain ();
            }

            SetCommand.create ( domain, attribute, ProtocolPackage.Literals.ATTRIBUTE__FIELD_NUMBER, b ).execute ();

            b++;
        }

        return null;
    }
}
