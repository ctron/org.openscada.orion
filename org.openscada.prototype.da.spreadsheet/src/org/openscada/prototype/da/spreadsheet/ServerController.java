/*
 * This file is part of the openSCADA project
 * Copyright (C) 2011-2012 TH4 SYSTEMS GmbH (http://th4-systems.com)
 *
 * openSCADA is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License version 3
 * only, as published by the Free Software Foundation.
 *
 * openSCADA is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License version 3 for more details
 * (a copy is included in the LICENSE file that accompanied this code).
 *
 * You should have received a copy of the GNU Lesser General Public License
 * version 3 along with openSCADA. If not, see
 * <http://opensource.org/licenses/lgpl-3.0.html> for a copy of the LGPLv3 License.
 */

package org.openscada.prototype.da.spreadsheet;

import java.util.Collection;
import java.util.HashSet;

import org.eclipse.core.databinding.observable.Realm;
import org.eclipse.core.databinding.observable.set.IObservableSet;
import org.eclipse.core.databinding.observable.set.WritableSet;
import org.eclipse.core.databinding.observable.value.IObservableValue;
import org.eclipse.core.databinding.observable.value.WritableValue;
import org.eclipse.ui.console.MessageConsole;
import org.eclipse.ui.statushandlers.StatusManager;
import org.openscada.core.ConnectionInformation;
import org.openscada.da.server.common.impl.HiveCommon;
import org.openscada.da.server.net.Exporter;
import org.openscada.ui.utils.status.StatusHelper;

public class ServerController
{
    private final WritableSet list;

    private ConnectionInformation connectionInformation;

    private HiveCommon hive;

    private Exporter exporter;

    private final WritableValue runningState;

    private final WritableSet grantedItems;

    public ServerController ( final Realm realm )
    {
        this.list = new WritableSet ( realm );
        this.runningState = new WritableValue ( realm, Boolean.FALSE, Boolean.class );
        this.grantedItems = new WritableSet ( realm, new HashSet<Object> (), String.class );
    }

    public void dispose ()
    {
        stop ();
        this.list.dispose ();
    }

    public IObservableValue getRunningState ()
    {
        return this.runningState;
    }

    public IObservableSet getGrantedItems ()
    {
        return this.grantedItems;
    }

    public IObservableSet getItems ()
    {
        return this.list;
    }

    public void setItems ( final Collection<PrototypeItem> items )
    {
        this.list.clear ();
        this.list.addAll ( items );
    }

    public void setConnectionInformation ( final ConnectionInformation connectionInformation )
    {
        this.connectionInformation = connectionInformation;
    }

    public void start ( final MessageConsole messageConsole )
    {
        if ( this.connectionInformation == null || this.hive != null || this.exporter != null )
        {
            return;
        }

        try
        {
            this.hive = new HiveImpl ( this.list, this.grantedItems, messageConsole );
            this.hive.start ();

            this.exporter = new Exporter ( this.hive, this.connectionInformation );
            this.exporter.start ();

            this.runningState.setValue ( true );
        }
        catch ( final Exception e )
        {
            StatusManager.getManager ().handle ( StatusHelper.convertStatus ( Activator.PLUGIN_ID, e ), StatusManager.SHOW );
        }
    }

    public void stop ()
    {
        if ( this.exporter != null )
        {
            try
            {
                this.exporter.stop ();
            }
            catch ( final Exception e )
            {
                StatusManager.getManager ().handle ( StatusHelper.convertStatus ( Activator.PLUGIN_ID, e ), StatusManager.SHOW );
            }
            finally
            {
                this.exporter = null;
            }
        }
        if ( this.hive != null )
        {
            try
            {
                this.hive.stop ();
            }
            catch ( final Exception e )
            {
                StatusManager.getManager ().handle ( StatusHelper.convertStatus ( Activator.PLUGIN_ID, e ), StatusManager.SHOW );
            }
            finally
            {
                this.hive = null;
            }
        }

        this.runningState.setValue ( false );
    }

    public boolean isStarted ()
    {
        return this.hive != null;
    }
}
