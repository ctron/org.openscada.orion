/**
 * <copyright>
 * </copyright>
 *
 * $Id$
 */
package org.openscada.prototype.da.spreadsheet.model.SpreadseetController.impl;

import org.eclipse.emf.common.notify.Notification;

import org.eclipse.emf.ecore.EClass;

import org.eclipse.emf.ecore.impl.ENotificationImpl;
import org.eclipse.emf.ecore.impl.EObjectImpl;

import org.openscada.prototype.da.spreadsheet.model.SpreadseetController.Controller;
import org.openscada.prototype.da.spreadsheet.model.SpreadseetController.NamingMode;
import org.openscada.prototype.da.spreadsheet.model.SpreadseetController.SpreadseetControllerPackage;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Controller</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * <ul>
 *   <li>{@link org.openscada.prototype.da.spreadsheet.model.SpreadseetController.impl.ControllerImpl#getSpreadsheet <em>Spreadsheet</em>}</li>
 *   <li>{@link org.openscada.prototype.da.spreadsheet.model.SpreadseetController.impl.ControllerImpl#getConnectionEndpoint <em>Connection Endpoint</em>}</li>
 *   <li>{@link org.openscada.prototype.da.spreadsheet.model.SpreadseetController.impl.ControllerImpl#getNamingMode <em>Naming Mode</em>}</li>
 * </ul>
 * </p>
 *
 * @generated
 */
public class ControllerImpl extends EObjectImpl implements Controller
{
    /**
     * The default value of the '{@link #getSpreadsheet() <em>Spreadsheet</em>}' attribute.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @see #getSpreadsheet()
     * @generated
     * @ordered
     */
    protected static final String SPREADSHEET_EDEFAULT = null;

    /**
     * The cached value of the '{@link #getSpreadsheet() <em>Spreadsheet</em>}' attribute.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @see #getSpreadsheet()
     * @generated
     * @ordered
     */
    protected String spreadsheet = SPREADSHEET_EDEFAULT;

    /**
     * The default value of the '{@link #getConnectionEndpoint() <em>Connection Endpoint</em>}' attribute.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @see #getConnectionEndpoint()
     * @generated
     * @ordered
     */
    protected static final String CONNECTION_ENDPOINT_EDEFAULT = null;

    /**
     * The cached value of the '{@link #getConnectionEndpoint() <em>Connection Endpoint</em>}' attribute.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @see #getConnectionEndpoint()
     * @generated
     * @ordered
     */
    protected String connectionEndpoint = CONNECTION_ENDPOINT_EDEFAULT;

    /**
     * The default value of the '{@link #getNamingMode() <em>Naming Mode</em>}' attribute.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @see #getNamingMode()
     * @generated
     * @ordered
     */
    protected static final NamingMode NAMING_MODE_EDEFAULT = NamingMode.ALIAS;

    /**
     * The cached value of the '{@link #getNamingMode() <em>Naming Mode</em>}' attribute.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @see #getNamingMode()
     * @generated
     * @ordered
     */
    protected NamingMode namingMode = NAMING_MODE_EDEFAULT;

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    protected ControllerImpl ()
    {
        super ();
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    protected EClass eStaticClass ()
    {
        return SpreadseetControllerPackage.Literals.CONTROLLER;
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    public String getSpreadsheet ()
    {
        return spreadsheet;
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    public void setSpreadsheet ( String newSpreadsheet )
    {
        String oldSpreadsheet = spreadsheet;
        spreadsheet = newSpreadsheet;
        if ( eNotificationRequired () )
            eNotify ( new ENotificationImpl ( this, Notification.SET, SpreadseetControllerPackage.CONTROLLER__SPREADSHEET, oldSpreadsheet, spreadsheet ) );
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    public String getConnectionEndpoint ()
    {
        return connectionEndpoint;
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    public void setConnectionEndpoint ( String newConnectionEndpoint )
    {
        String oldConnectionEndpoint = connectionEndpoint;
        connectionEndpoint = newConnectionEndpoint;
        if ( eNotificationRequired () )
            eNotify ( new ENotificationImpl ( this, Notification.SET, SpreadseetControllerPackage.CONTROLLER__CONNECTION_ENDPOINT, oldConnectionEndpoint, connectionEndpoint ) );
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    public NamingMode getNamingMode ()
    {
        return namingMode;
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    public void setNamingMode ( NamingMode newNamingMode )
    {
        NamingMode oldNamingMode = namingMode;
        namingMode = newNamingMode == null ? NAMING_MODE_EDEFAULT : newNamingMode;
        if ( eNotificationRequired () )
            eNotify ( new ENotificationImpl ( this, Notification.SET, SpreadseetControllerPackage.CONTROLLER__NAMING_MODE, oldNamingMode, namingMode ) );
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public Object eGet ( int featureID, boolean resolve, boolean coreType )
    {
        switch ( featureID )
        {
            case SpreadseetControllerPackage.CONTROLLER__SPREADSHEET:
                return getSpreadsheet ();
            case SpreadseetControllerPackage.CONTROLLER__CONNECTION_ENDPOINT:
                return getConnectionEndpoint ();
            case SpreadseetControllerPackage.CONTROLLER__NAMING_MODE:
                return getNamingMode ();
        }
        return super.eGet ( featureID, resolve, coreType );
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public void eSet ( int featureID, Object newValue )
    {
        switch ( featureID )
        {
            case SpreadseetControllerPackage.CONTROLLER__SPREADSHEET:
                setSpreadsheet ( (String)newValue );
                return;
            case SpreadseetControllerPackage.CONTROLLER__CONNECTION_ENDPOINT:
                setConnectionEndpoint ( (String)newValue );
                return;
            case SpreadseetControllerPackage.CONTROLLER__NAMING_MODE:
                setNamingMode ( (NamingMode)newValue );
                return;
        }
        super.eSet ( featureID, newValue );
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public void eUnset ( int featureID )
    {
        switch ( featureID )
        {
            case SpreadseetControllerPackage.CONTROLLER__SPREADSHEET:
                setSpreadsheet ( SPREADSHEET_EDEFAULT );
                return;
            case SpreadseetControllerPackage.CONTROLLER__CONNECTION_ENDPOINT:
                setConnectionEndpoint ( CONNECTION_ENDPOINT_EDEFAULT );
                return;
            case SpreadseetControllerPackage.CONTROLLER__NAMING_MODE:
                setNamingMode ( NAMING_MODE_EDEFAULT );
                return;
        }
        super.eUnset ( featureID );
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public boolean eIsSet ( int featureID )
    {
        switch ( featureID )
        {
            case SpreadseetControllerPackage.CONTROLLER__SPREADSHEET:
                return SPREADSHEET_EDEFAULT == null ? spreadsheet != null : !SPREADSHEET_EDEFAULT.equals ( spreadsheet );
            case SpreadseetControllerPackage.CONTROLLER__CONNECTION_ENDPOINT:
                return CONNECTION_ENDPOINT_EDEFAULT == null ? connectionEndpoint != null : !CONNECTION_ENDPOINT_EDEFAULT.equals ( connectionEndpoint );
            case SpreadseetControllerPackage.CONTROLLER__NAMING_MODE:
                return namingMode != NAMING_MODE_EDEFAULT;
        }
        return super.eIsSet ( featureID );
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public String toString ()
    {
        if ( eIsProxy () )
            return super.toString ();

        StringBuffer result = new StringBuffer ( super.toString () );
        result.append ( " (spreadsheet: " );
        result.append ( spreadsheet );
        result.append ( ", connectionEndpoint: " );
        result.append ( connectionEndpoint );
        result.append ( ", namingMode: " );
        result.append ( namingMode );
        result.append ( ')' );
        return result.toString ();
    }

} //ControllerImpl
