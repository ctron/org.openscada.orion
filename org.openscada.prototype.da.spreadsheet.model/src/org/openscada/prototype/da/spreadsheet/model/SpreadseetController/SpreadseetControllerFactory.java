/**
 * <copyright>
 * </copyright>
 *
 * $Id$
 */
package org.openscada.prototype.da.spreadsheet.model.SpreadseetController;

import org.eclipse.emf.ecore.EFactory;

/**
 * <!-- begin-user-doc -->
 * The <b>Factory</b> for the model.
 * It provides a create method for each non-abstract class of the model.
 * <!-- end-user-doc -->
 * @see org.openscada.prototype.da.spreadsheet.model.SpreadseetController.SpreadseetControllerPackage
 * @generated
 */
public interface SpreadseetControllerFactory extends EFactory
{
    /**
     * The singleton instance of the factory.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    SpreadseetControllerFactory eINSTANCE = org.openscada.prototype.da.spreadsheet.model.SpreadseetController.impl.SpreadseetControllerFactoryImpl.init ();

    /**
     * Returns a new object of class '<em>Controller</em>'.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @return a new object of class '<em>Controller</em>'.
     * @generated
     */
    Controller createController ();

    /**
     * Returns the package supported by this factory.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @return the package supported by this factory.
     * @generated
     */
    SpreadseetControllerPackage getSpreadseetControllerPackage ();

} //SpreadseetControllerFactory
