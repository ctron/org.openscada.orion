/*
 * This file is part of the openSCADA project
 * 
 * Copyright (C) 2011-2012 TH4 SYSTEMS GmbH (http://th4-systems.com)
 * Copyright (C) 2013 Jens Reimann (ctron@dentrassi.de)
 *
 * openSCADA is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License version 3
 * only, as published by the Free Software Foundation.
 *
 * openSCADA is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License version 3 for more details
 * (a copy is included in the LICENSE file that accompanied this code).
 *
 * You should have received a copy of the GNU Lesser General Public License
 * version 3 along with openSCADA. If not, see
 * <http://opensource.org/licenses/lgpl-3.0.html> for a copy of the LGPLv3 License.
 */

package org.openscada.prototype.da.spreadsheet;

import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

import org.eclipse.ui.console.MessageConsoleStream;
import org.openscada.core.Variant;
import org.openscada.core.VariantEditor;
import org.openscada.core.server.OperationParameters;
import org.openscada.da.core.WriteAttributeResults;
import org.openscada.da.core.WriteResult;
import org.openscada.da.server.browser.common.query.ItemDescriptor;
import org.openscada.da.server.common.AttributeMode;
import org.openscada.da.server.common.DataItemInformationBase;
import org.openscada.da.server.common.DataItemInputCommon;
import org.openscada.da.server.common.ItemListener;
import org.openscada.da.server.common.WriteAttributesHelper;
import org.openscada.utils.concurrent.InstantFuture;
import org.openscada.utils.concurrent.NotifyFuture;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class SimulationItem extends DataItemInputCommon
{

    private final static Logger logger = LoggerFactory.getLogger ( SimulationItem.class );

    private Variant value = Variant.NULL;

    private final ItemDescriptor descriptor;

    private final PropertyChangeListener propertyListener;

    private final PrototypeItem prototypeItem;

    private final MessageConsoleStream messageStream;

    public SimulationItem ( final PrototypeItem prototypeItem, final MessageConsoleStream messageStream )
    {
        super ( new DataItemInformationBase ( prototypeItem.getItemId () ) );

        this.prototypeItem = prototypeItem;
        this.messageStream = messageStream;

        final Map<String, Variant> descriptorAttributes = new HashMap<String, Variant> ( 1 );
        descriptorAttributes.put ( "description", Variant.valueOf ( prototypeItem.getItem ().getDescription () ) );
        this.descriptor = new ItemDescriptor ( this, descriptorAttributes );

        this.propertyListener = new PropertyChangeListener () {

            @Override
            public void propertyChange ( final PropertyChangeEvent evt )
            {
                handlePropertyChange ( evt );
            }
        };
        prototypeItem.addPropertyChangeListener ( this.propertyListener );

        setValue ( VariantEditor.toVariant ( prototypeItem.getItem ().getSimulationValue () ) );
    }

    @Override
    public synchronized void setListener ( final ItemListener listener )
    {
        super.setListener ( listener );
        this.prototypeItem.setSubscribed ( listener != null );
    }

    @Override
    public NotifyFuture<WriteResult> startWriteValue ( final Variant value, final OperationParameters operationParameters )
    {
        this.messageStream.println ( String.format ( "%tc - WRITE - Value: %s, OperationParameters: %s", new Date (), value, operationParameters ) );
        return new InstantFuture<WriteResult> ( WriteResult.OK );
    }

    @Override
    public NotifyFuture<WriteAttributeResults> startSetAttributes ( final Map<String, Variant> attributes, final OperationParameters operationParameters )
    {
        final Date now = new Date ();
        this.messageStream.println ( String.format ( "%tc - SETATTR - OperationParameters: %s", now, operationParameters ) );

        for ( final Map.Entry<String, Variant> entry : attributes.entrySet () )
        {
            this.messageStream.println ( String.format ( "%tc         - Name: %s, Value: %s", now, entry.getKey (), entry.getValue (), operationParameters ) );
        }

        return new InstantFuture<WriteAttributeResults> ( WriteAttributesHelper.okUnhandled ( null, attributes ) );
    }

    protected void handlePropertyChange ( final PropertyChangeEvent evt )
    {
        try
        {
            if ( PrototypeItem.PROP_SIMULATION_VALUE.equals ( evt.getPropertyName () ) )
            {
                setValue ( (Variant)evt.getNewValue () );
            }
            else if ( PrototypeItem.PROP_SIMULATION_ERROR_FLAG.equals ( evt.getPropertyName () ) )
            {
                setFlagValue ( "error", (Boolean)evt.getNewValue () );
            }
            else if ( PrototypeItem.PROP_SIMULATION_ALARM_FLAG.equals ( evt.getPropertyName () ) )
            {
                setFlagValue ( "alarm", (Boolean)evt.getNewValue () );
            }
            else if ( PrototypeItem.PROP_SIMULATION_MANUAL_FLAG.equals ( evt.getPropertyName () ) )
            {
                setFlagValue ( "manual", (Boolean)evt.getNewValue () );
            }
            else if ( PrototypeItem.PROP_SIMULATION_BLOCK_FLAG.equals ( evt.getPropertyName () ) )
            {
                setFlagValue ( "block", (Boolean)evt.getNewValue () );
            }
            else if ( PrototypeItem.PROP_SIMULATION_ACK_FLAG.equals ( evt.getPropertyName () ) )
            {
                setFlagValue ( "ackRequired", (Boolean)evt.getNewValue () );
            }
        }
        catch ( final Exception e )
        {
            logger.warn ( "Failed to set property", e );
        }
    }

    private void setFlagValue ( final String attribute, final Boolean value )
    {
        final Map<String, Variant> attributes = new HashMap<String, Variant> ( 1 );
        attributes.put ( attribute, Variant.valueOf ( value ) );
        updateData ( null, attributes, AttributeMode.UPDATE );
    }

    public void setValue ( final Variant value )
    {
        if ( this.value == null || !this.value.equals ( value ) )
        {
            this.value = value;
            final Map<String, Variant> attributes = new HashMap<String, Variant> ( 1 );
            attributes.put ( "timestamp", Variant.valueOf ( System.currentTimeMillis () ) );
            updateData ( value, attributes, AttributeMode.UPDATE );
        }
    }

    public ItemDescriptor getDescriptor ()
    {
        return this.descriptor;
    }

    public void dispose ()
    {
        this.prototypeItem.removePropertyChangeListener ( this.propertyListener );
    }
}
